<?php

namespace App\Http\Controllers;

use App\Events\UserRegistered;
use Illuminate\Http\Request;
use Auth;
use Laravel\Socialite\Facades\Socialite;
use Hash;
use Str;
use App\Models\User;
use Illuminate\Support\Facades\Input;

class SocialController extends Controller
{

    public function redirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function Callback($provider){

        //Check User with provider's email is present or not
        $userSocial =   Socialite::driver($provider)->stateless()->user();
        $users       =   User::where(['email' => $userSocial->getEmail()])->first();

        //If user is present then login otherwise SignUp
        if($users){
            Auth::login($users);
            return redirect('/home');
        }else{
            $user = User::create([
                'name'          => $userSocial->getName(),
                'email'         => $userSocial->getEmail(),
                'provider'      => $provider,
                'provider_id'   => $userSocial->getId(),

            ]);

            //call event for sent welcome email after successful registration
            event(new UserRegistered($user));

            $users =  User::where(['email' => $userSocial->getEmail()])->first();
            Auth::login($users);
            return redirect('/home');
        }
    }

    //method use for check validation
    public function checkEmailExists(){
        $user = User::all()->where('email', Input::get('email'))->first();
        if ($user) {
            return "false";
        }else{
            return "true";
        }
    }

    //method use for check validation
    public function checkEmailExistsForResetPass(){
        $user = User::all()->where('email', Input::get('email'))->first();
        if ($user) {
            return "true";
        }else{
            return "false";
        }
    }
}
