# Laravel authentication demo

A demo application to illustrate Laravel Authentication System.

> This Snippet contains Laravel Authentication using Jquery Validation and design made using Bootstrap.

![](https://i.postimg.cc/Kv42MMHf/Register.png)

> It has social signin/signup functionality and it has been done using Laravel Socialite Package. In this example, you can check signin/signup with google and github.

> Events and Listeners are there in the system which will automatically send email on Successful signup, No matter which way you are doing signup.

![](https://i.postimg.cc/SNQKtRtY/Log-in.png)
