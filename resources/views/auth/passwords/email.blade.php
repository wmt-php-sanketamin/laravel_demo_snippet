<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Forgot password</title>

    <!-- Global stylesheets -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w==" crossorigin="anonymous" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/assets/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/assets/bootstrap_limitless.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/assets/layout.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/assets/components.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/assets/colors.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script src="{{ asset('js/main/jquery.min.js') }}"></script>
    <script src="{{ asset('js/main/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('js/plugins/loaders/blockui.min.js') }}"></script>

    <!-- Theme JS files -->
    <script src="{{ asset('js/demo_pages/login.js') }}"></script>
    <script src="{{ asset('js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script src="{{ asset('js/assets/app.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.js"></script>
    <!-- /theme JS files -->

</head>

<body>

<div class="page-content login-cover">

    <!-- Main content -->
    <div class="content-wrapper">

        <!-- Content area -->
        <div class="content d-flex justify-content-center align-items-center">

            <!-- Forgot password form -->
            <form class="login-form wmin-sm-400" id="email-form" method="POST" action="{{ route('password.email') }}">
                @csrf

                <div class="card mb-0">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if(Session::has('message'))
                        <p class="alert alert-success">{{ Session::get('message') }}</p>
                    @endif
                    <div class="tab-content card-body">
                        <div class="tab-pane fade show active" id="login-tab1">
                            <div class="text-center mb-3">
                                <i class="icon-spinner11 icon-2x text-danger border-danger border-3 rounded-round p-3 mb-3 mt-1"></i>
                                <h5 class="mb-0">Reset Password</h5>
                                <span class="d-block text-muted">We'll send you Password reset link in email</span>
                            </div>
                            @if(Session::has('message'))
                                <p class="alert alert-danger">{{ Session::get('message') }}</p>
                            @endif
                            <div class="form-group form-group-feedback form-group-feedback-right">
                                <input type="email" id="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Enter your email">
                                <div class="form-control-feedback">
                                    <i class="icon-mail5 text-muted"></i>
                                </div>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        {{ __('Send Password Reset Link') }}
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <!-- /Forgot password form -->

        </div>
        <!-- /content area -->

    </div>
    <!-- /main content -->

</div>
<!-- /page content -->

</body>
<script>
    $('#email-form').validate({
        //Rules For Validation
        rules: {
            email: {
                "required" : true,
                'email' : true,
                'remote':{
                    url:"{{route('check_email_exists_for_resetpass')}}",
                    type:"get"
                }
            },
        },
        //Messages For Failed Validation
        messages: {
            email: {
                "required" : "Email is required",
                'remote': "Email not Found in our database"
            },
        }
    });
</script>
</html>
