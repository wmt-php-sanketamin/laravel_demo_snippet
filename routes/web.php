<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

//Laravel Authentication Routes
Auth::routes();

//Default Homepage route
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//Socialite Login/SignUp Route
Route::get('login/{provider}', [App\Http\Controllers\SocialController::class, 'redirect']);
Route::get('login/{provider}/callback',[App\Http\Controllers\SocialController::class, 'Callback']);

//Route for validation use
Route::get('checkEmailExists',[App\Http\Controllers\SocialController::class, 'checkEmailExists'])->name('check_email_exists');
Route::get('checkEmailExistsForResetPass',[App\Http\Controllers\SocialController::class, 'checkEmailExistsForResetPass'])->name('check_email_exists_for_resetpass');
